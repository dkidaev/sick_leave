import os

from dotenv import load_dotenv
from telebot import TeleBot

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

token = os.getenv("TOKEN")

bot: TeleBot = TeleBot(token)

personal = {
    'phone': None,
    'first_name' : None,
    'type': None
}
patient = {
    'surname': None,
    'name': None,
    'middle_name': None,
    'birthdate': None,
    'diagnostic': None,
    'doctor_open':None,
    'number_list':None,
    'date_open':None,
    'doctor_close':None,
    'disability': None,
    'date_close':None,
    'index': None

}