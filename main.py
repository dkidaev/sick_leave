from telebot import types

from config import bot, personal, patient
from datetime import datetime
today = datetime.today().strftime('%d-%m-%Y')


@bot.message_handler(commands=['start'])
def phone(message):
    keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
    button = types.KeyboardButton(text="Авторизация", request_contact=True)
    keyboard.add(button)
    bot.send_message(message.chat.id, 'Для продолжения вы должны авторизоваться', reply_markup=keyboard)


@bot.message_handler(content_types=['contact'])
def contact(message):
    with open('admin.txt', 'r') as file:
        data_file = [line.rstrip().split(', ') for line in file.readlines()]
        print(data_file)
        for person in data_file:
            if person[0] == message.contact.phone_number:
                bot.send_message(message.chat.id, f'Доступ предоставлен.'
                                                  f'\n Здравствуйте {message.contact.first_name}')
                personal['phone'] = person[0]
                personal['first_name'] = person[1]
                personal['type'] = person[2]
    menu(message)


@bot.message_handler(content_types=['text'])
def get_text_message(message):
    if personal['phone']:
        if message.text == "Регистрация":
            msg = bot.send_message(message.chat.id, "Фамилия Имя Отчество")
            bot.register_next_step_handler(msg, regist_birthdate)
        elif message.text == "Поиск пациента":
            msg = bot.send_message(message.chat.id, "Введите Фамилию:")
            bot.register_next_step_handler(msg, find_surname)
    else:
        bot.send_message(message.chat.id, 'Необходимо авторизоваться')
        phone(message)


def menu(message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    buttons = ["Поиск пациента", "Регистрация"]
    keyboard.add(*buttons[:2])
    bot.send_message(message.chat.id, 'Выберите действие:', reply_markup=keyboard)


def regist_birthdate(message):
    full_name = message.text
    patient['surname'] = full_name.split(' ')[0].upper()
    patient['name'] = full_name.split(' ')[1].upper()
    patient['middle_name'] = full_name.split(' ')[2].upper()
    print(patient)
    msg = bot.send_message(message.chat.id, "Введите: Дату рождения(дд-мм-гггг): ")
    bot.register_next_step_handler(msg, regist_close)

def find_surname(message):
    name = message.text
    with open('bd.txt', 'r') as file:
        for index, line in enumerate(file.readlines()):
            for iline in line.split(':'):
                if iline == name.upper():
                    patient['surname'] = line.split(':')[0]
                    patient['name'] = line.split(':')[1]
                    patient['middle_name'] = line.split(':')[2]
                    patient['birthdate'] = line.split(':')[3].rstrip()
                    patient['index'] = index
                    patient['diagnostic'] = None
                    patient['doctor_open'] = None
                    patient['doctor_close'] = None
                    patient['disability'] = None
    if patient['number_list']:
        text = f'Фамилия:' + patient['surname'] \
               + f'\nИмя: ' + patient['name'] \
               + f'\nОтчество: ' + patient['middle_name'] \
               + f'\nДата рождения: ' + patient['birthdate']\
               + f'\nНом.больничного: ' + patient['number_list'] \
               + f'\nДиагноз: ' + patient['diagnostic']\
               + f'\nВрач: ' + patient['diagnostic'] \
               + f'\nДата открытия: ' + patient['date_open'] \
               + f'\nДата закрытия: ' + patient['date_close']
    else:
        text = f'Фамилия:' + patient['surname']\
               + f'\nИмя: ' + patient['name'] \
               + f'\nОтчество: ' + patient['middle_name'] \
               + f'\nДата рождения: ' + patient['birthdate']
    bot.send_message(message.chat.id, 'Такой пациент найден.')
    bot.send_message(message.chat.id, text)

    print(personal['type'])

    if personal['type'] == 'врач':
        open_number_list(message)

def open_number_list(message):
    keyboard = types.InlineKeyboardMarkup()
    button1 = types.InlineKeyboardButton(text='Открыть БЛ', callback_data=f'list:open')
    button2 = types.InlineKeyboardButton(text='Закрыть БЛ', callback_data=f'list:close')
    keyboard.add(button1, button2)
    bot.send_message(message.chat.id, 'Выберите действие: ', reply_markup=keyboard)

def open_doctor(message):
    patient['number_list'] = message.text
    patient['doctor_open'] = personal['first_name']
    msg = bot.send_message(message.chat.id, 'Диагноз: ')
    bot.register_next_step_handler(msg, diagnostic)

def diagnostic(message):
    patient['diagnostic'] = message.text
    patient['date_open'] = today
    resave(message)

def regist_close(message):
    patient['birthdate'] = message.text
    save(message)


def save(message):
    keyboard = types.InlineKeyboardMarkup()
    button1 = types.InlineKeyboardButton(text='Cохранить', callback_data=f'save:yes')
    button2 = types.InlineKeyboardButton(text='Отмена', callback_data=f'save:no')
    keyboard.add(button1, button2)
    bot.send_message(message.chat.id, 'Сохранить данные в файл: ', reply_markup=keyboard)

def resave(message):
    keyboard = types.InlineKeyboardMarkup()
    button1 = types.InlineKeyboardButton(text='Cохранить', callback_data=f'resave:yes')
    button2 = types.InlineKeyboardButton(text='Отмена', callback_data=f'resave:no')
    keyboard.add(button1, button2)
    bot.send_message(message.chat.id, 'Сохранить данные в файл: ', reply_markup=keyboard)

def clear():
    patient['surname'] = None
    patient['name'] = None
    patient['middle_name'] = None
    patient['birthdate'] = None
    patient['diagnostic'] = None
    patient['doctor_open'] = None
    patient['number_list'] = None
    patient['date_open'] = None
    patient['doctor_close'] = None
    patient['disability'] = None
    patient['date_close'] = None
    patient['index'] = None


@bot.callback_query_handler(func=lambda call: True)
def callback_worker(call):
    if call.data.startswith('save'):
        if call.data[5:] == 'yes':
            with open('bd.txt', 'a+') as file:
                file.write(patient['surname'] + ":"
                           + patient['name']+ ":"
                           + patient['middle_name']+ ":"
                           + patient['birthdate'] + "\n")
            bot.edit_message_reply_markup(call.message.chat.id, call.message.message_id)
            bot.send_message(call.message.chat.id, "👍")
        elif call.data[5:] == 'no':
            patient['surname'] = None
            patient['name'] = None
            patient['middle_name'] = None
            patient['birthdate'] = None
            bot.send_message(call.message.chat.id, "👍")
    elif call.data.startswith('list'):
        print(call.data[5:])
        if call.data[5:] == 'open':
            msg = bot.send_message(call.message.chat.id, 'Введите номер больничного: ')
            bot.register_next_step_handler(msg, open_doctor)
        elif call.data[5:] == 'close':
            msg = bot.send_message(call.message.chat.id, 'Введите номер больничного: ')
            bot.register_next_step_handler(msg, open_doctor)
    elif call.data.startswith('resave'):
        print(call.data[7:])
        if call.data[7:] == 'yes':
            text = patient['surname'] + ":" \
                   + patient['name'] + ":" \
                   + patient['middle_name'] + ":" \
                   + patient['birthdate'] + ":" \
                   + patient['diagnostic'] + ":"\
                   + patient['doctor_open'] + ":"\
                   + patient['number_list'] + ":"\
                   + patient['date_open'] + "\n"
            print(text)
            with open('bd.txt', 'a+') as file, open('temp.txt', 'w') as file2:
                file.seek(0)
                lines = file.readlines()
                print(lines)
                del lines [patient['index']]
                print(lines)
                for iline in lines:
                    file2.write(iline)
            with open('bd.txt', 'w') as file, open ('temp.txt', 'r') as file2:
                file.write(text)
                for iline in file2.readlines():
                    file.write(iline)
                    bot.send_message(call.message.chat.id, "👍")
                    clear()




bot.polling(none_stop=True, interval=0)
